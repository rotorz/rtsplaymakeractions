README
======

These [PlayMaker][PlayMaker] action scripts are designed for use with [Rotorz
Tile System][RTS] which allow non-programmers to interact with tile systems at
runtime. The objective of these actions is to provide key functionality for
interacting with tile systems at runtime rather than to simply expose all
functions, properties and fields.

Licensed under the MIT license. See LICENSE file in the project root for full license
information. DO NOT contribute to this project unless you accept the terms of the
contribution agreement.

**Minimum required version of Rotorz Tile System:** 2.3.1

**Minimum required version of PlayMaker: **1.6.0

[RTS]: http://rotorz.com/tilesystem/
[PlayMaker]: http://www.hutonggames.com/

Installing scripts
------------------

Add action scripts somewhere within the "Assets" folder of your project. For
organisational purposes the following path might be most appropriate:

    {Your Project}/Assets/RTS_Extras/PlayMaker/

The following video demonstrates how to paint tiles using RtsPlayMakerActions:

[Painting Tiles using PlayMaker](<http://youtu.be/GzQADvf6-KY>)

Useful links
------------

- [Action Reference (Wiki)](<https://bitbucket.org/rotorz/rtsplaymakeractions/wiki>)
- [Rotorz Tile System](<http://rotorz.com/tilesystem>)
- [PlayMaker](<http://www.hutonggames.com/>)
- [Rotorz Website](<http://rotorz.com>)

Contribution Agreement
----------------------

This project is licensed under the MIT license (see LICENSE). To be in the best
position to enforce these licenses the copyright status of this project needs to
be as simple as possible. To achieve this the following terms and conditions
must be met:

- All contributed content (including but not limited to source code, text,
  image, videos, bug reports, suggestions, ideas, etc.) must be the
  contributors own work.

- The contributor disclaims all copyright and accepts that their contributed
  content will be released to the public domain.

- The act of submitting a contribution indicates that the contributor agrees
  with this agreement. This includes (but is not limited to) pull requests, issues,
  tickets, e-mails, newsgroups, blogs, forums, etc.