// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using HutongGames.PlayMakerEditor;
using Rotorz.Tile.Editor;
using System;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Rotorz.Tile.PlayMakerActions {

	public abstract class PaintingUtilityActionEditor : CustomActionEditor {

		#region Custom GUI

		private static Func<Rect, int, int> s_RotationSelector;

		private static GUIContent s_TempContent = new GUIContent();

		private static Rect PrefixLabel(Rect totalPosition, string label) {
			s_TempContent.text = label;
			return EditorGUI.PrefixLabel(totalPosition, GUIUtility.GetControlID(FocusType.Passive), s_TempContent);
		}

		private static bool UseVariableToggle(Rect position, bool value) {
			return GUI.Toggle(position, value, FsmEditorContent.VariableButton, FsmEditorStyles.MiniButtonPadded);
		}

		protected void EnumField(string variableName, FsmInt variable, Type enumType) {
			if (!variable.UseVariable) {
				Rect position = EditorGUILayout.GetControlRect();
				position.width -= 17 + 3;
				variable.Value = EditorGUI.IntPopup(position, ObjectNames.NicifyVariableName(variableName), variable.Value, Enum.GetNames(enumType), Enum.GetValues(enumType).Cast<int>().ToArray());

				position = new Rect(position.xMax + 3, position.y, 17, 14);
				variable.UseVariable = UseVariableToggle(position, variable.UseVariable);
			}
			else {
				EditField(variableName);
			}
		}

		#endregion

		public PaintingUtilityActionEditor() {
			var staticBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
			var miRotationSelector = typeof(RotorzEditorGUI).GetMethod("RotationSelector", staticBindingFlags, null, new Type[] { typeof(Rect), typeof(int) }, null);
			if (miRotationSelector != null)
				s_RotationSelector = Delegate.CreateDelegate(typeof(Func<Rect, int, int>), miRotationSelector) as Func<Rect, int, int>;
		}

		protected bool DrawPaintingUtilityFields() {
			var action = target as PaintingUtilityAction;
			bool isDirty = false;

			isDirty |= EditVariationField();

			RotorzEditorGUI.SeparatorLight();

			isDirty |= EditRotationField();

			// Avoid showing the following properties for the flood fill action.
			if (!(action is FloodFill)) {
				RotorzEditorGUI.SeparatorLight();

				EditField("paintAroundExistingTiles");

				if (!action.fillRatePercentage.UseVariable) {
					Rect position = EditorGUILayout.GetControlRect();
					position.width -= 17 + 3;
					action.fillRatePercentage.Value = EditorGUI.IntSlider(position, "Fill Rate Percentage", action.fillRatePercentage.Value, 0, 100);

					position = new Rect(position.xMax + 3, position.y, 17, 14);
					action.fillRatePercentage.UseVariable = UseVariableToggle(position, action.fillRatePercentage.UseVariable);
				}
				else {
					EditField("fillRatePercentage");
				}
			}

			return isDirty || GUI.changed;
		}

		private bool EditVariationField() {
			var action = target as PaintingUtilityAction;
			bool isDirty = false;

			bool randomizeVariation = action.variationIndex.Value == -1;

			EditField("variationIndex");
			++EditorGUI.indentLevel;

			EditorGUI.BeginChangeCheck();
			randomizeVariation = EditorGUILayout.Toggle("Randomize Variation", randomizeVariation);
			if (EditorGUI.EndChangeCheck())
				action.variationIndex.Value = randomizeVariation ? -1 : 0;

			EditField("variationShiftCount");

			--EditorGUI.indentLevel;

			return isDirty || GUI.changed;
		}

		private bool EditRotationField() {
			var action = target as PaintingUtilityAction;
			bool isDirty = false;

			Color restoreColor = GUI.color;

			if (!action.rotation.UseVariable) {
				Rect totalPosition = PrefixLabel(EditorGUILayout.GetControlRect(true, 36), "Rotation");

				Rect position = totalPosition;
				if (action.randomizeRotation.Value)
					GUI.color = new Color(1f, 1f, 1f, 0.45f);
				position.x -= 13;
				position.width = 39;
				int newRotation = s_RotationSelector(position, action.rotation.Value);
				if (!action.randomizeRotation.Value)
					action.rotation.Value = newRotation;
				GUI.color = restoreColor;

				position = new Rect(totalPosition.xMax - 17, totalPosition.y, 17, 14);
				action.rotation.UseVariable = UseVariableToggle(position, action.rotation.UseVariable);
			}
			else {
				EditField("rotation");
			}

			++EditorGUI.indentLevel;

			EditField("randomizeRotation");

			--EditorGUI.indentLevel;

			return isDirty || GUI.changed;
		}

	}

}