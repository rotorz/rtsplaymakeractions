// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMakerEditor;
using UnityEngine;

namespace Rotorz.Tile.PlayMakerActions {

	[ObjectPropertyDrawer(typeof(Brush))]
	public class BrushPropertyDrawer_PlayMaker : ObjectPropertyDrawer {
		
		public override Object OnGUI(GUIContent label, Object obj, bool isSceneObject, params object[] attributes) {
			return Editor.RotorzEditorGUI.BrushField(label.text, obj as Brush, true, false);
		}

	}

}