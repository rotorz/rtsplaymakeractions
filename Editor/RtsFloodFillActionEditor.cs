// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMakerEditor;
using Rotorz.Tile.Editor;
using UnityEditor;
using UnityEngine;

namespace Rotorz.Tile.PlayMakerActions {

	[CustomActionEditor(typeof(FloodFill))]
	public class FloodFillActionEditor : PaintingUtilityActionEditor {

		public override bool OnGUI() {
			var action = target as FloodFill;
			bool isDirty = false;

			EditField("tileSystemComponent");
			EditField("brush");

			RotorzEditorGUI.SeparatorLight();

			EditField("rowIndex");
			EditField("columnIndex");

			EditorGUILayout.Space();

			EditField("maximumFillCount");

			RotorzEditorGUI.SeparatorLight();

			isDirty |= DrawPaintingUtilityFields();

			return isDirty || GUI.changed;
		}

	}

}