// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMakerEditor;
using Rotorz.Tile.Editor;
using UnityEditor;
using UnityEngine;

namespace Rotorz.Tile.PlayMakerActions {

	[CustomActionEditor(typeof(PaintLine))]
	public class PaintLineActionEditor : PaintingUtilityActionEditor {

		public override bool OnGUI() {
			var action = target as PaintLine;
			bool isDirty = false;

			EditField("tileSystemComponent");
			EditField("brush");

			RotorzEditorGUI.SeparatorLight();

			EditField("fromRowIndex");
			EditField("fromColumnIndex");
			EditField("toRowIndex");
			EditField("toColumnIndex");

			EditorGUILayout.Space();

			EnumField("nozzleType", action.nozzleType, typeof(BrushNozzle));
			EditField("nozzleSize");

			RotorzEditorGUI.SeparatorLight();

			isDirty |= DrawPaintingUtilityFields();

			return isDirty || GUI.changed;
		}

	}

}