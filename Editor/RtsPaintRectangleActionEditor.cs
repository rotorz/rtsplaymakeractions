// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMakerEditor;
using Rotorz.Tile.Editor;
using UnityEditor;
using UnityEngine;

namespace Rotorz.Tile.PlayMakerActions {

	[CustomActionEditor(typeof(PaintRectangle))]
	public class PaintRectangleActionEditor : PaintingUtilityActionEditor {

		public override bool OnGUI() {
			bool isDirty = false;

			EditField("tileSystemComponent");
			EditField("brush");

			RotorzEditorGUI.SeparatorLight();

			EditField("fromRowIndex");
			EditField("fromColumnIndex");
			EditField("toRowIndex");
			EditField("toColumnIndex");

			EditorGUILayout.Space();

			EditField("outlineOnly");

			RotorzEditorGUI.SeparatorLight();

			isDirty |= DrawPaintingUtilityFields();

			return isDirty || GUI.changed;
		}

	}

}