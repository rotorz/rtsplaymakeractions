// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Gets chunk component that contains a specific tile.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/GetChunkFromTileIndex")]
	public class GetChunkFromTileIndex : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[ObjectType(typeof(Chunk)), UIHint(UIHint.Variable)]
		[Tooltip("The chunk component or null if chunk does not exist.")]
		public FsmObject chunkComponent;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			everyFrame = false;
			
			chunkComponent = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			chunkComponent.Value = tileSystem.GetChunkFromTileIndex(rowIndex.Value, columnIndex.Value);
		}
		
	}
	
}