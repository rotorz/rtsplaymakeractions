// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Gets chunk component from tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/GetChunk")]
	public class GetChunk : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based index of chunk row.")]
		public FsmInt chunkRowIndex;
		[RequiredField]
		[Tooltip("Zero-based index of chunk column.")]
		public FsmInt chunkColumnIndex;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[ObjectType(typeof(Chunk)), UIHint(UIHint.Variable)]
		[Tooltip("The chunk component or null if chunk does not exist.")]
		public FsmObject chunkComponent;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			chunkRowIndex = null;
			chunkColumnIndex = null;
			everyFrame = false;
			
			chunkComponent = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			chunkComponent.Value = tileSystem.GetChunk(chunkRowIndex.Value, chunkColumnIndex.Value);
		}
		
	}
	
}