// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Sets the state of a custom user flag.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/SetTileUserFlag")]
	public class SetTileUserFlag : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		[RequiredField]
		[Tooltip("The custom user flag (1 to 16).")]
		public FsmInt flagNumber;

		[Tooltip("Indicates if flag should be set or unset.")]
		public FsmBool setFlag;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			flagNumber = 1;
			setFlag = null;
			everyFrame = false;
		}
		
		public override void OnEnter() {
			DoSetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoSetValue();
		}
		
		private void DoSetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;
			
			TileData tile = tileSystem.GetTile(rowIndex.Value, columnIndex.Value);
			tile.SetUserFlag(flagNumber.Value, setFlag.Value);
		}
		
	}
	
}