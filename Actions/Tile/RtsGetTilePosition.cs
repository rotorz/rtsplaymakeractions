// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Gets position of a tile in local space (of tile system) or world space.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/GetTilePosition")]
	public class GetTilePosition : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		
		[RequiredField]
		public Space space;
		[Tooltip("Indicates part of tile to retrieve position from.")]
		public PositionFrom positionFrom;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[RequiredField, UIHint(UIHint.Variable)]
		[Tooltip("Position of tile.")]
		public FsmVector3 position;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			space = Space.World;
			positionFrom = PositionFrom.Center;
			everyFrame = false;
			
			position = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;
			
			position.Value = (space == Space.World)
				? tileSystem.WorldPositionFromTileIndex(rowIndex.Value, columnIndex.Value, positionFrom == PositionFrom.Center)
				: tileSystem.LocalPositionFromTileIndex(rowIndex.Value, columnIndex.Value, positionFrom == PositionFrom.Center);
		}
		
	}
	
}