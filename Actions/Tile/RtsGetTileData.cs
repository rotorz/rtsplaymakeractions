// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Gets data for a specific tile.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/GetTileData")]
	public class GetTileData : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Indicates if tile is empty.")]
		public FsmBool isEmpty;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Indicates if tile has been flagged as solid.")]
		public FsmBool isSolid;
		
		[ObjectType(typeof(Brush)), UIHint(UIHint.Variable)]
		[Tooltip("Brush that was used to paint tile (alway `null` if stripped).")]
		public FsmObject brush;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Variation of tile.")]
		public FsmInt variationIndex;
		
		[ObjectType(typeof(Tileset)), UIHint(UIHint.Variable)]
		[Tooltip("The associated tileset (if any).")]
		public FsmObject tileset;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Zero-based index of tile in its tileset.")]
		public FsmInt tilesetIndex;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("The attached game object (if any).")]
		public FsmGameObject gameObject;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			everyFrame = false;
			
			isEmpty = null;
			isSolid = null;
			brush = null;
			variationIndex = null;
			tileset = null;
			tilesetIndex = null;
			gameObject = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;
			
			TileData tile = tileSystem.GetTile(rowIndex.Value, columnIndex.Value);
			if (tile != null) {
				isEmpty.Value = false;
				isSolid.Value = tile.SolidFlag;
				brush.Value = tile.brush;
				variationIndex.Value = tile.variationIndex;
				gameObject.Value = tile.gameObject;
				
				if (tileset.UseVariable)
					tileset.Value = tile.tileset;
				if (tilesetIndex.UseVariable)
					tilesetIndex.Value = tile.tilesetIndex;
			}
			else {
				isEmpty.Value = true;
				isSolid.Value = false;
				brush.Value = null;
				variationIndex.Value = 0;
				gameObject.Value = null;
				
				tileset.Value = null;
				tilesetIndex.Value = -1;
			}
		}
		
	}
	
}