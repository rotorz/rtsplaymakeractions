// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Gets row and column of tile closest from a world position.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/ClosestTileIndexFromWorld")]
	public class ClosestTileIndexFromWorld : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Some position in world space.")]
		public FsmVector3 worldPosition;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			worldPosition = null;
			everyFrame = false;
			
			rowIndex = null;
			columnIndex = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			TileIndex index = tileSystem.ClosestTileIndexFromWorld(worldPosition.Value);
			
			rowIndex.Value = index.row;
			columnIndex.Value = index.column;
		}
		
	}
	
}