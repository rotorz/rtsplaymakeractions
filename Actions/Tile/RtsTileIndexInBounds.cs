// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Determines whether a tile index is within bounds of tile system grid.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/GetTileData")]
	public class TileIndexInBounds : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile.")]
		public FsmInt columnIndex;
		
		[Tooltip("Repeat this action every frame.")]
		public bool everyFrame;
		
		#endregion
		
		#region Outputs
		
		[Tooltip("Event to send if tile index is in bounds.")]
		public FsmEvent trueEvent;
		
		[Tooltip("Event to send if tile index is NOT in bounds.")]
		public FsmEvent falseEvent;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Store the result in a bool variable.")]
		public FsmBool storeResult;

		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			everyFrame = false;
			
			trueEvent = null;
			falseEvent = null;
			storeResult = null;
		}
		
		public override void OnEnter() {
			DoGetValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate() {
			DoGetValue();
		}
		
		private void DoGetValue() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			bool inBounds = tileSystem.InBounds(rowIndex.Value, columnIndex.Value);

			storeResult.Value = inBounds;
			
			Fsm.Event(inBounds ? trueEvent : falseEvent);
		}
		
	}
	
}