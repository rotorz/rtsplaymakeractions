// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Creates a brand new tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/CreateTileSystem")]
	public class CreateTileSystem : FsmStateAction {
		
		#region Inputs
		
		[Tooltip("Name for new tile system.")]
		public FsmString name;
		
		[RequiredField]
		[Tooltip("Size of each individual tile.")]
		public FsmVector3 tileSize;
		
		[RequiredField]
		[Tooltip("Number of rows in tile system.")]
		public FsmInt rowCount;
		[RequiredField]
		[Tooltip("Number of columns in tile system.")]
		public FsmInt columnCount;
		
		[RequiredField]
		[Tooltip("Number of columns of tiles in a chunk.")]
		public FsmInt chunkWidth;
		[RequiredField]
		[Tooltip("Number of rows of tiles in a chunk.")]
		public FsmInt chunkHeight;
		
		#endregion
		
		#region Outputs
		
		[UIHint(UIHint.Variable)]
		[Tooltip("New tile system game object.")]
		public FsmGameObject gameObject;
		
		[ObjectType(typeof(TileSystem)), UIHint(UIHint.Variable)]
		[Tooltip("New tile system component.")]
		public FsmObject tileSystemComponent;
		
		#endregion
		
		public override void Reset() {
			name = null;
			tileSize = Vector3.one;
			rowCount = 100;
			columnCount = 100;
			chunkWidth = 30;
			chunkHeight = 30;
			
			gameObject = null;
			tileSystemComponent = null;
		}
		
		public override void OnEnter() {
			GameObject go = new GameObject(!string.IsNullOrEmpty(name.Value) ? name.Value : "Untitled Tile System");
			
			TileSystem tileSystem = go.AddComponent<TileSystem>();
			tileSystem.CreateSystem(
				tileSize.Value.x, tileSize.Value.y, tileSize.Value.z,
				rowCount.Value, columnCount.Value,
				chunkWidth.Value, chunkHeight.Value
			);
			
			gameObject.Value = go;
			tileSystemComponent.Value = tileSystem;
			
			Finish();
		}
		
	}
	
}