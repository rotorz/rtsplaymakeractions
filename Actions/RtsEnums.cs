// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

namespace Rotorz.Tile.PlayMakerActions {
	
	public enum PositionFrom {
		Center,
		Edge
	}
	
	public enum Space {
		World,
		Local
	}
	
}