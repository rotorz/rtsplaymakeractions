// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Erases a specific tile in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/EraseTile")]
	public class EraseTile : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile to erase.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile to erase.")]
		public FsmInt columnIndex;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			tileSystem.EraseTile(rowIndex.Value, columnIndex.Value);
			tileSystem.RefreshSurroundingTiles(rowIndex.Value, columnIndex.Value);
			
			Finish();
		}
		
	}
	
}