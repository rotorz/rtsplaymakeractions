// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Begins bulk edit mode for a tile system. Use this to improve performance when painting and/or erasing multiple tiles in bulk. Changes will be finalised upon invoking `EndBulkEdit`.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/BeginBulkEdit")]
	public class BeginBulkEdit : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			tileSystem.BeginBulkEdit();
			
			Finish();
		}
		
	}
	
}