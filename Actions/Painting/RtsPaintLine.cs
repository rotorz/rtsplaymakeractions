// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[Tooltip("Paints a line of tiles connecting two points using the specified brush in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/PaintLine")]
	public class PaintLine : PaintingUtilityAction {
		
		#region Inputs
		
		[RequiredField]
		[Tooltip("Zero-based row of 'from' tile of line.")]
		public FsmInt fromRowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of 'from' tile of line.")]
		public FsmInt fromColumnIndex;
		[RequiredField]
		[Tooltip("Zero-based row of 'to' tile of line.")]
		public FsmInt toRowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of 'to' tile of line.")]
		public FsmInt toColumnIndex;

		[RequiredField]
		[Tooltip("Indicates the type of nozzle to stroke line with (Default: Square).")]
		public FsmInt nozzleType;
		[RequiredField]
		[Tooltip("Size or radius of nozzle.")]
		public FsmInt nozzleSize;

		#endregion
		
		public override void Reset() {
			base.Reset();

			fromRowIndex = 0;
			fromColumnIndex = 0;
			toRowIndex = 0;
			toColumnIndex = 0;
			nozzleType = (int)BrushNozzle.Square;
			nozzleSize = 1;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			TileIndex fromIndex = new TileIndex(fromRowIndex.Value, fromColumnIndex.Value);
			TileIndex toIndex = new TileIndex(toRowIndex.Value, toColumnIndex.Value);
			BrushNozzle nozzle = (BrushNozzle)nozzleType.Value;

			var paintingArgs = GetPaintingArgs(brush.Value as Brush);

			if (nozzleSize.Value > 1) {
				switch (nozzle) {
					default:
					case BrushNozzle.Square:
						PaintingUtility.StrokeLineWithSquare(tileSystem, fromIndex, toIndex, nozzleSize.Value, paintingArgs);
						break;

					case BrushNozzle.Round:
						PaintingUtility.StrokeLineWithCircle(tileSystem, fromIndex, toIndex, nozzleSize.Value, paintingArgs);
						break;
				}
			}
			else {
				PaintingUtility.PaintLine(tileSystem, fromIndex, toIndex, paintingArgs);
			}

			Finish();
		}
		
	}

}