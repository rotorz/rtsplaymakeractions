// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[Tooltip("Fills an area of tiles using the specified brush in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/FloodFill")]
	public class FloodFill : PaintingUtilityAction {
		
		#region Inputs
		
		[RequiredField]
		[Tooltip("Zero-based row of tile to begin filling from.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column tile to begin filling from.")]
		public FsmInt columnIndex;

		[RequiredField]
		[Tooltip("Limits the number of tiles painted in a single filling action (Default: 100). Set to 0 to disable limitation.")]
		public FsmInt maximumFillCount;

		#endregion
		
		public override void Reset() {
			base.Reset();

			rowIndex = 0;
			columnIndex = 0;
			maximumFillCount = 100;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			int restoreMaximumFillCount = PaintingUtility.MaximumFillCount;
			PaintingUtility.MaximumFillCount = maximumFillCount.Value;
			try {
				TileIndex index = new TileIndex(rowIndex.Value, columnIndex.Value);

				var paintingArgs = GetPaintingArgs(brush.Value as Brush);
				PaintingUtility.FloodFill(tileSystem, index, paintingArgs);
			}
			finally {
				PaintingUtility.MaximumFillCount = restoreMaximumFillCount;
			}

			Finish();
		}
		
	}

}