// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Cycles to specific variation of tile if possible.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/CycleTile")]
	public class CycleTile : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile to paint.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile to paint.")]
		public FsmInt columnIndex;
		
		[RequiredField]
		[Tooltip("Zero-based index of desired tile variation. Specify -1 for randomisation.")]
		public FsmInt variationIndex;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			rowIndex = null;
			columnIndex = null;
			variationIndex = -1;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;
			
			TileData tile = tileSystem.GetTile(rowIndex.Value, columnIndex.Value);
			if (tile == null || tile.brush == null)
				return;
			
			int variation = (variationIndex.Value == -1)
				? Brush.RANDOM_VARIATION
				: variationIndex.Value;
			tile.brush.Cycle(tileSystem, rowIndex.Value, columnIndex.Value, variation);
			
			Finish();
		}
		
	}
	
}