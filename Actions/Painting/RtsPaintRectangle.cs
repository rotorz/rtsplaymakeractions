// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[Tooltip("Paints a rectangle of tiles using the specified brush in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/PaintRectangle")]
	public class PaintRectangle : PaintingUtilityAction {
		
		#region Inputs
		
		[RequiredField]
		[Tooltip("Zero-based row of 'from' tile of rectangle.")]
		public FsmInt fromRowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of 'from' tile of rectangle.")]
		public FsmInt fromColumnIndex;
		[RequiredField]
		[Tooltip("Zero-based row of 'to' tile of rectangle.")]
		public FsmInt toRowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of 'to' tile of rectangle.")]
		public FsmInt toColumnIndex;

		[RequiredField]
		[Tooltip("Indicates whether only an outline should be painted.")]
		public FsmBool outlineOnly;

		#endregion
		
		public override void Reset() {
			base.Reset();

			fromRowIndex = 0;
			fromColumnIndex = 0;
			toRowIndex = 0;
			toColumnIndex = 0;
			outlineOnly = false;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			TileIndex fromIndex = new TileIndex(fromRowIndex.Value, fromColumnIndex.Value);
			TileIndex toIndex = new TileIndex(toRowIndex.Value, toColumnIndex.Value);

			var paintingArgs = GetPaintingArgs(brush.Value as Brush);
			PaintingUtility.PaintRectangle(tileSystem, fromIndex, toIndex, !outlineOnly.Value, paintingArgs);

			Finish();
		}
		
	}

}