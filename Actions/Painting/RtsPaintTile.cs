// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Paints a tile using the specified brush in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/PaintTile")]
	public class PaintTile : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[ObjectType(typeof(Brush))]
		[Tooltip("The brush; a value of `null` indicates that tile should be erased.")]
		public FsmObject brush;
		
		[RequiredField]
		[Tooltip("Zero-based row of tile to paint.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of tile to paint.")]
		public FsmInt columnIndex;
		
		[RequiredField]
		[Tooltip("Zero-based index of desired tile variation. Specify -1 for randomization.")]
		public FsmInt variationIndex;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			brush = null;
			rowIndex = null;
			columnIndex = null;
			variationIndex = -1;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			Brush theBrush = brush.Value as Brush;
			
			if (theBrush != null) {
				int variation = (variationIndex.Value == -1)
					? Brush.RANDOM_VARIATION
					: variationIndex.Value;
				theBrush.Paint(tileSystem, rowIndex.Value, columnIndex.Value, variation);
			}
			else {
				tileSystem.EraseTile(rowIndex.Value, columnIndex.Value);
			}
			
			tileSystem.RefreshSurroundingTiles(rowIndex.Value, columnIndex.Value);
			
			Finish();
		}
		
	}
	
}