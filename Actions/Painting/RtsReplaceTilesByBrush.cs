// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	[Tooltip("Replaces all tiles painted using one brush with another brush. Please remember that this will not work if brush references have been stripped from tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/ReplaceTilesByBrush")]
	public class ReplaceTilesByBrush : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;
		
		[RequiredField, ObjectType(typeof(Brush))]
		[Tooltip("Find tiles that were painted using this brush.")]
		public FsmObject findBrush;
		[ObjectType(typeof(Brush))]
		[Tooltip("Repaint found tiles using this brush. Specify `null` to erase found tiles.")]
		public FsmObject replacementBrush;
		
		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			findBrush = null;
			replacementBrush = null;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			tileSystem.ReplaceByBrush(findBrush.Value as Brush, replacementBrush.Value as Brush);
			
			Finish();
		}
		
	}
	
}