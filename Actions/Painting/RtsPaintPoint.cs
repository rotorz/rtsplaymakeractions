// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[Tooltip("Paints a point of tiles using the specified brush in a tile system.")]
	[HelpUrl("https://bitbucket.org/rotorz/rtsplaymakeractions/wiki/Action/PaintPoint")]
	public class PaintPoint : PaintingUtilityAction {
		
		#region Inputs
		
		[RequiredField]
		[Tooltip("Zero-based row of point.")]
		public FsmInt rowIndex;
		[RequiredField]
		[Tooltip("Zero-based column of point.")]
		public FsmInt columnIndex;

		[RequiredField]
		[Tooltip("Indicates the type of nozzle to paint with.")]
		public FsmInt nozzleType;
		[RequiredField]
		[Tooltip("Size or radius of nozzle.")]
		public FsmInt nozzleSize;

		#endregion
		
		public override void Reset() {
			base.Reset();

			rowIndex = 0;
			columnIndex = 0;
			nozzleType = (int)BrushNozzle.Square;
			nozzleSize = 1;
		}
		
		public override void OnEnter() {
			TileSystem tileSystem = tileSystemComponent.Value as TileSystem;
			if (tileSystem == null)
				return;

			TileIndex index = new TileIndex(rowIndex.Value, columnIndex.Value);
			BrushNozzle nozzle = (BrushNozzle)nozzleType.Value;
			int size = Mathf.Max(1, nozzleSize.Value);

			var paintingArgs = GetPaintingArgs(brush.Value as Brush);

			switch (nozzle) {
				default:
				case BrushNozzle.Square:
					PaintingUtility.PaintSquare(tileSystem, index, size, paintingArgs);
					break;

				case BrushNozzle.Round:
					PaintingUtility.PaintCircle(tileSystem, index, size, paintingArgs);
					break;
			}

			Finish();
		}
		
	}

}