// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Rotorz.Tile.PlayMakerActions {
	
	[ActionCategory("Rotorz Tile System")]
	public abstract class PaintingUtilityAction : FsmStateAction {
		
		#region Inputs
		
		[RequiredField, ObjectType(typeof(TileSystem))]
		[Tooltip("The tile system component.")]
		public FsmObject tileSystemComponent;

		[ObjectType(typeof(Brush))]
		[Tooltip("Brush to use when painting tiles. Specify a value of `null` to erase tiles instead of painting them.")]
		public FsmObject brush;

		[RequiredField]
		[Tooltip("Zero-based index of desired tile variation. Specify -1 for randomization.")]
		public FsmInt variationIndex;
		[RequiredField]
		[Tooltip("Count of variations to shift by.")]
		public FsmInt variationShiftCount;
		
		[RequiredField]
		[Tooltip("Indicates whether new tiles should be painted around existing tiles rather than painting over them. This of course does not apply when erasing tiles.")]
		public FsmBool paintAroundExistingTiles;

		[RequiredField]
		[Tooltip("Indicates whether rotation should be randomized when painting tiles.")]
		public FsmBool randomizeRotation;
		[RequiredField]
		[Tooltip("Rotation of painted tiles.")]
		public FsmInt rotation;

		[RequiredField]
		[Tooltip("A value ranging between 0 and 100 (inclusive) which indicates the rate in which candidate tiles are filled. Specify a value lower than 100 to spray tiles onto a tile system.")]
		public FsmInt fillRatePercentage;

		#endregion
		
		public override void Reset() {
			tileSystemComponent = null;
			brush = null;
			variationIndex = 0;
			variationShiftCount = 0;
			paintAroundExistingTiles = false;
			randomizeRotation = false;
			rotation = 0;
			fillRatePercentage = 100;
		}
		
		protected PaintingArgs GetPaintingArgs(Brush brush) {
			var paintingArgs = PaintingArgs.GetDefaults(brush);

			paintingArgs.fillRatePercentage = Mathf.Clamp(fillRatePercentage.Value, 0, 100);
			paintingArgs.paintAroundExistingTiles = paintAroundExistingTiles.Value;
			paintingArgs.randomizeRotation = randomizeRotation.Value;
			paintingArgs.rotation = rotation.Value;

			paintingArgs.variation = variationIndex.Value;
			if (paintingArgs.variation == -1)
				paintingArgs.variation = Brush.RANDOM_VARIATION;

			paintingArgs.variationShiftCount = variationShiftCount.Value;

			return paintingArgs;
		}
		
	}

}